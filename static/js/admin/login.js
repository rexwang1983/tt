angular.module('App', []).controller('loginCtrl', function($scope, $http) {
        $scope.login = function() { 
            $http({
                method: 'POST',
                url: '/api/admin/login',
                data: {
                   username: $scope.username,
                   password: $scope.password,
                   remember: $scope.remember,
                }
            }).then(function(response) {
                if(response.data.success){
                   window.location = response.data.url;
                }
                else {
                    $scope.erroMessage = response.data.errorMsg;
                }
                           }, function(error) {
                $scope.erroMessage = 'There is something wrong, please contact admin';
            });
        }
    })

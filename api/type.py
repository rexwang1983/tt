import json
import sys
sys.path.append("..")
from models import Type


def get_types():
    return json.dumps({'success': True, 'types': Type.find_all()})

def create(data):
    t = Type(Name=data['name'])
    t.insert()
    return json.dumps({'success': True})


def update(data):
    t = Type.find_first('where Id=?', data['id'])
    t.Name = data['name']
    t.update()
    return json.dumps({'success': True})


def remove(data):
    t = Type.find_first('where Id=?', data['id'])
    t.delete()
    return json.dumps({'success': True})

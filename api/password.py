import base64
import hashlib


def encode(message):
    return base64.b64encode(hashlib.sha1(message).digest())

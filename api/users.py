import json
from . import password
import time
import sys
sys.path.append("..")
from models import User

def add(data):
    pwd = password.encode(data['password'])
    u = User(Name=data['username'], Password=pwd)
    a = u.insert()
    return json.dumps({'success': True})


def login(data,IP):
    pwd = password.encode(data['password'])
    u = User.find_first('where Name=?', data['username'])
    if u is None:
        return json.dumps({'success': False,'errorMsg': 'Name not find'})
    if u['Password'] == pwd:
        u['LastLoginTime'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        u['LastLoginIP'] = str(IP)
        u.update()
        return json.dumps({'success': True,'url': '/admin', 'id': u['Id'], 'email': u['Email']})
    else:
        return json.dumps({'success': False,'errorMsg': 'passworad is wrong'})


def get_account_info(id):
    u = User.find_first('where Id=?', id)
    return json.dumps({'success': True, 'email': u.Email})


def update(id, data):
    u = User.find_first('where Id=?', id)
    u.Email = data['email']
    u.update()
    return json.dumps({'success': True})


def change_password(id, data):
    u = User.find_first('where Id=?', id)
    old_pwd = password.encode(data['oldPassword'])
    if u['Password'] != old_pwd:
        return json.dumps({'success': False,'errorMsg': 'Old passworad is wrong'})
    pwd = password.encode(data['password'])
    u.Password = pwd
    u.update()
    return json.dumps({'success': True})

angular.module('App', [])
.controller('font_indexCtrl', function($scope, $http) {
            $http({
                method: 'POST',
                url: '/api/admin/getarticlenumber',
                data: {
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.article_number = response.data.article_number;
                }
                else {
                    console.log(response.data.errorMsg);
                }
                           }, function(error) {
               console.log(error);
            });

            $http({
                method: 'POST',
                url: '/api/admin/searchtop',
                data: {
                  number:'8',
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.articles = response.data["articles"];
                }
                else {
                }
                           }, function(error) {
               console.log(error);
            });

            $http({
                method: 'POST',
                url: '/api/admin/gettypes',
                data: {
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.types = response.data.types;
                }
                else {
                    alert(response.data.errorMsg);
                }
                           }, function(error) {
               console.log(error);
            });
            $scope.search = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/searcharticle',
                  data: {
                     title:"",
                     typeId:$scope.type,
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        $scope.articles = response.data["articles"];
                     }
                     else{
                        
                     }
              }, function(error) {
                  
              });
            }
    })
.filter('htmlContent',['$sce', function($sce) {
  return function(input) {
    return $sce.trustAsHtml(input);
  }
}]);

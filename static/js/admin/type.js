angular.module('App', []).controller('typeCtrl', function($scope, $http) {
            $scope.refersh = function(){
                $http({
                    method: 'POST',
                    url: '/api/admin/gettypes',
                    data: {
                    }
                }).then(function(response) {
                      if(response.data.success){
                       $scope.types = response.data.types;
                    }
                    else {
                        alert(response.data.errorMsg);
                    }
                               }, function(error) {
                        console.log(error);
                });
            }
            $scope.update = function(id,name) {
              $http({
                  method: 'POST',
                  url: '/api/admin/updatetype',
                  data: {
                     id: id,
                     name:name
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        $scope.refersh();
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert(error);
              });
            }
            $scope.remove = function(id) {
              $http({
                  method: 'POST',
                  url: '/api/admin/removetype',
                  data: {
                     id: id
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        $scope.refersh();
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert(error);
              });
            }
            $scope.createType = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/createtype',
                  data: {
                     name: $scope.name
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        alert("创建成功！");

                        $scope.refersh();
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert("创建失败！");
              });
            }
            $scope.refersh();
    })

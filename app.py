# coding: utf8

from flask import Flask, render_template, request
from flask import redirect, url_for
from flask_login import login_required, current_user
from flask_login import LoginManager, login_user, logout_user
import json
import conf
import api
import orm
from user import User

app = Flask(__name__)
# make jinja use %% start and end, make sure angular can use {{}}
app.jinja_env.variable_start_string = '%%'
app.jinja_env.variable_end_string = '%%'

app.secret_key = conf.secret_key

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "/admin/login"

orm.db.create_engine(
    user= conf.configs['db']['user'],
    password= conf.configs['db']['password'],
    database= conf.configs['db']['database'],
    host= conf.configs['db']['host'],
    port= conf.configs['db']['port'],)


@login_manager.user_loader
def load_user(user_id):
    user = User()
    user.id = user_id
    return user


# page route

# admin
# @app.route('/admin/adduser')
# def admin_add_user():
#     return render_template('/admin/temp_add_user.html')


@app.route('/admin/login')
def admin_login():
    return render_template('/admin/login.html')


@app.route('/admin')
@login_required
def admin_index():
    return render_template('/admin/index.html')


@app.route('/admin/create')
@login_required
def admin_article_create():
    return render_template('/admin/create.html')


@app.route('/admin/type')
@login_required
def admin_type():
    return render_template('/admin/type.html')


@app.route('/admin/account')
@login_required
def admin_account():
    return render_template('/admin/account.html')


@app.route('/admin/article')
@login_required
def admin_article():
    return render_template('/admin/article.html')


# font
@app.route('/')
def index():
    return render_template('/index.html')


@app.route('/page')
def page():
    return render_template('/page.html')


# api route
# @app.route('/api/admin/adduser', methods=['POST'])
# def api_add_user():
#     try:
#         return api.users.add(request.json)
#     except Exception as e:
#         return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/getaccountinfo', methods=['POST'])
@login_required
def get_account_info():
    try:
        return api.users.get_account_info(current_user.id)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/updateaccount', methods=['POST'])
@login_required
def update_account():
    try:
        return api.users.update(current_user.id, request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/changepassword', methods=['POST'])
@login_required
def change_password():
    try:
        return api.users.change_password(current_user.id, request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/getarticlenumber', methods=['POST'])
def get_article_number():
    try:
        return api.article.get_number()
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/createarticle', methods=['POST'])
@login_required
def create_article():
    try:
        return api.article.create(request.json, current_user.id)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/getarticle', methods=['POST'])
def get_article():
    try:
        return api.article.get(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/updatearticle', methods=['POST'])
@login_required
def update_article():
    try:
        return api.article.update(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/deletearticle', methods=['POST'])
@login_required
def delete_article():
    try:
        return api.article.delete(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/searcharticle', methods=['POST'])
def search_article():
    try:
        return api.article.search(request.json, current_user.id)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/searchtop', methods=['POST'])
def search_top():
    try:
        return api.article.search_top(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/gettypes', methods=['POST'])
def get_types():
    try:
        return api.type.get_types()
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/createtype', methods=['POST'])
@login_required
def create_type():
    try:
        return api.type.create(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/updatetype', methods=['POST'])
@login_required
def update_type():
    try:
        return api.type.update(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/removetype', methods=['POST'])
@login_required
def remove_type():
    try:
        return api.type.remove(request.json)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/login', methods=['POST'])
def api_login():
    try:
        result = json.loads(api.users.login(request.json, request.remote_addr))
        if(result['success']):
            user = User()
            user.id = result['id']
            user.email = result['email']
            login_user(user)
        return json.dumps(result)
    except Exception as e:
        return json.dumps({'success': False, 'errorMsg': str(e)})


@app.route('/api/admin/logout', methods=["GET", "POST"])
def api_logout():
    logout_user()
    return redirect('/admin/login')

if __name__ == "__main__":
    app.run(host= conf.host, port= conf.port)


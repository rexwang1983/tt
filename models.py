# -*- coding: utf-8 -*-


import time, uuid
from orm.orm import Model, StringField, BooleanField, FloatField, TextField, IntegerField


class User(Model):
    __table__ = 'User'

    Id = IntegerField(primary_key=True)
    Email = StringField(ddl='varchar(255)')
    Password = StringField(ddl='varchar(255)')
    Name = StringField(ddl='varchar(255)')
    LastLoginTime = FloatField(default=time.time)
    LastLoginIP = StringField(ddl='varchar(255)')


class Article(Model):
    __table__ = 'Article'

    Id = IntegerField(primary_key=True)
    Title = StringField(ddl='varchar(255)')
    TypeId = IntegerField()
    Content = TextField()
    IsPublic = BooleanField()
    CreateTime = StringField()
    LastUpdateTime = StringField()
    AuthorId = IntegerField()
    UpdateInfo = StringField(ddl='varchar(1000)')
    Year = IntegerField()
    Month = IntegerField()
    Day = IntegerField()


class Type(Model):
    __table__ = 'Type'

    Id = IntegerField(primary_key=True)
    Name = StringField(ddl='varchar(255)')


class Comment(Model):
    __table__ = 'Comment'

    Id = IntegerField(primary_key=True)
    ArticleId = IntegerField()
    Content = TextField()
    Name = StringField(ddl='varchar(255)')
    CreateTime = FloatField(default=time.time)

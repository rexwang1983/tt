'use strict';
var angular = window.angular, appName = 'App';

angular.module(appName, [
          'simditor'      
        ])
       .controller('createCtrl', function($scope, $http) {
            $scope.content = '';
            $scope.isPublic = false;
            $scope.type = 0;
            var myDate = new Date();
            $scope.year = myDate.getFullYear() - 2017;
            $scope.month = myDate.getMonth() -3; //month从0开始
            if($scope.month<=0){
              $scope.month+=12;
            }
            $scope.day =  myDate.getDate() - 21;
            if($scope.day<=0){
              $scope.day+=31;
            }
            $http({
                method: 'POST',
                url: '/api/admin/gettypes',
                data: {
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.types = response.data.types;
                }
                else {
                    alert(response.data.errorMsg);
                }
                           }, function(error) {
               console.log(error);
            });

            $scope.create = function() {
              console.log($scope.content);
              $http({
                  method: 'POST',
                  url: '/api/admin/createarticle',
                  data: {
                     title: $scope.title,
                     typeId:$scope.type,
                     content:$scope.content,
                     isPublic:$scope.isPublic,
                     year:$scope.year,
                     month:$scope.month,
                     day:$scope.day,
                     time:myDate.toLocaleString()
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        alert("创建成功！");
                        window.location = response.data.url;
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert("创建失败！");
              });
            }

            $scope.createType = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/createtype',
                  data: {
                     name: $scope.name
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        alert("创建成功！");

                        $http({
                          method: 'POST',
                          url: '/api/admin/gettypes',
                          data: {
                          }
                          }).then(function(response) {
                                if(response.data.success){
                                 $scope.types = response.data.types;
                              }
                              else {
                                  alert(response.data.errorMsg);
                              }
                                         }, function(error) {
                             console.log(error);
                          });
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert("创建失败！");
              });
            }
    })

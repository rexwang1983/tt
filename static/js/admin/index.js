angular.module('App', [])
.controller('indexCtrl', function($scope, $http) {
            $scope.dateStartChecked = false;
            $scope.dateEndChecked = false;
            $scope.onlyMine = false;
            $scope.type = 0;
            $scope.title = '';
            $http({
                method: 'POST',
                url: '/api/admin/getarticlenumber',
                data: {
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.article_number = response.data.article_number;
                }
                else {
                    alert(response.data.errorMsg);
                }
                           }, function(error) {
               console.log(error);
            });

            $http({
                method: 'POST',
                url: '/api/admin/searchtop',
                data: {
                  number:'10',
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.articles = response.data["articles"];
                }
                else {
                }
                           }, function(error) {
               console.log(error);
            });

            $http({
                method: 'POST',
                url: '/api/admin/gettypes',
                data: {
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.types = response.data.types;
                }
                else {
                    alert(response.data.errorMsg);
                }
                           }, function(error) {
               console.log(error);
            });
            $scope.search = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/searcharticle',
                  data: {
                     title:$scope.title,
                     typeId:$scope.type,
                     // dateStartChecked:$scope.dateStartChecked,
                     // dateEndChecked:$scope.dateEndChecked,
                     // dateStart:$scope.dateStart,
                     // dateEnd:$scope.dateEnd,
                     // onlyMine:$scope.onlyMine
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        $scope.articles = response.data["articles"];
                     }
                     else{
                        
                     }
              }, function(error) {
                  alert(error);
              });
            }
    })
.filter('htmlContent',['$sce', function($sce) {
  return function(input) {
    return $sce.trustAsHtml(input);
  }
}]);

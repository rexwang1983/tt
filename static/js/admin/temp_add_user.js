angular.module('App', []).controller('addUserCtrl', function($scope, $http) {
        $scope.add = function() { 
            $http({
                method: 'POST',
                url: '/api/admin/adduser',
                data: {
                   username: $scope.username,
                   password: $scope.password,
                }
            }).then(function(response) {
                if(response.data.success){
                   $scope.erroMessage = "success";
                }
                else {
                    $scope.erroMessage = response.data.errorMsg;
                }
                           }, function(error) {
                $scope.erroMessage = 'There is something wrong, please contact admin';
            });
        }
    })

import json
import time
import sys
sys.path.append("..")
from models import Article


def get_number():
    return json.dumps({'success': True, 'article_number': Article.count_all()})


def create(data,user_id):
    a = Article(
             Title= data['title'],
             TypeId= data['typeId'],
             Content= data['content'],
             IsPublic= data['isPublic'],
             # CreateTime= str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())),
             CreateTime = data['time'],
             Year = data['year'],
             Month = data['month'],
             Day = data['day'],
             AuthorId= user_id
        )
    a.insert()
    return json.dumps({'success': True, 'url': '/admin'})


def get(data):
    a = Article.find_first('where Id=?', data['id'])
    if a is None:
        return json.dumps({'success': False, 'errorMsg': 'Article not find'})
    return json.dumps({
        'success': True,
        'id': a.Id,
        'title': a.Title,
        'isPublic': a.IsPublic,
        'type': a.TypeId,
        'content': a.Content,
        'year': a.Year,
        'month': a.Month,
        'day': a.Day,
        })


def update(data):
    a = Article.find_first('where Id=?', data['id'])
    a.Title = data['title']
    a.TypeId = data['typeId']
    a.Content = data['content']
    a.IsPublic = data['isPublic']
    # a.LastUpdateTime = str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    a.LastUpdateTime = data['time']
    a.Year = data['year']
    a.Month = data['month']
    a.Day = data['day']
    a.update()
    return json.dumps({'success': True})


def delete(data):
    a = Article.find_first('where Id=?', data['id'])
    a.delete()
    return json.dumps({'success': True, 'url': '/admin'})


def search(data, user_id):
    condition = "where Title like '%" + data['title'] + "%'"
    if data['typeId'] != 0:
        condition += ' and TypeId=' + str(data['typeId'])
    # if data['dateStartChecked']:
    #     condition += ' and CreateTime>' + data['dateStart']
    # if data['dateEndChecked']
    #     condition += ' and CreateTime<' + data['dateEnd']
    # if data['onlyMine']:
    #     condition += ' and AuthorId=' + str(user_id)
    a = Article.find_by(condition)
    return json.dumps({'success': True, 'articles': a})


def search_top(data):
    a = Article.find_define('select * from Article order by id desc limit ' + data['number'])
    return json.dumps({'success': True, 'articles': a})

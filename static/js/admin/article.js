'use strict';
var angular = window.angular, appName = 'App';

angular.module(appName, [
          'simditor'      
        ])
      .controller('createCtrl', function($scope, $http) {
          $scope.id = 0;
          var myDate = new Date();
          var r = window.location.search.substr(1)
          if (r != null){
              $scope.id = parseInt(r);
            }
            $http({
                method: 'POST',
                url: '/api/admin/gettypes',
                data: {
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.types = response.data.types;
                }
                else {
                    alert(response.data.errorMsg);
                }
                           }, function(error) {
               console.log(error);
            });
            $http({
                method: 'POST',
                url: '/api/admin/getarticle',
                data: {
                  id:$scope.id
                }
            }).then(function(response) {
                  if(response.data.success){
                   $scope.type = response.data.type;
                   $scope.title = response.data.title;
                   $scope.isPublic = response.data.isPublic;
                   $scope.content = response.data.content;
                   $scope.id = response.data.id;
                   $scope.year = response.data.year;
                   $scope.month = response.data.month;
                   $scope.day = response.data.day;
                }
                else {
                    alert(response.data.errorMsg);
                    $scope.id = 0;
                }
                           }, function(error) {
               console.log(error);
            });

            $scope.update = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/updatearticle',
                  data: {
                     id:$scope.id,
                     title:$scope.title,
                     typeId:$scope.type,
                     content:$scope.content,
                     isPublic:$scope.isPublic,
                     year:$scope.year,
                     month:$scope.month,
                     day:$scope.day,
                     time:myDate.toLocaleString()
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        alert("更新成功！");
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert("更新失败！");
              });
            }

            $scope.delete = function() {
              if(confirm("是否删除"+$scope.title+"？")){
                  $http({
                      method: 'POST',
                      url: '/api/admin/deletearticle',
                      data: {
                         id:$scope.id,
                      }
                  }).then(function(response) {
                         if(response.data["success"]){
                            alert("删除成功！");
                            window.location = response.data.url;
                         }
                         else{
                            alert(response.data["errorMsg"]);
                         }
                  }, function(error) {
                      alert("删除失败！");
                  });
              }
            }

            $scope.createType = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/createtype',
                  data: {
                     name: $scope.name
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        alert("创建成功！");

                        $http({
                          method: 'POST',
                          url: '/api/admin/gettypes',
                          data: {
                          }
                          }).then(function(response) {
                                if(response.data.success){
                                 $scope.types = response.data.types;
                              }
                              else {
                                  alert(response.data.errorMsg);
                              }
                                         }, function(error) {
                             console.log(error);
                          });
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert("创建失败！");
              });
            }

    })

angular.module('App', []).controller('accountCtrl', function($scope, $http) {
            $scope.refersh = function(){
                $http({
                    method: 'POST',
                    url: '/api/admin/getaccountinfo',
                    data: {
                    }
                }).then(function(response) {
                      if(response.data.success){
                       $scope.email = response.data.email;
                    }
                    else {
                        alert(response.data.errorMsg);
                    }
                               }, function(error) {
                        console.log(error);
                });
            }
            $scope.update = function() {
              $http({
                  method: 'POST',
                  url: '/api/admin/updateaccount',
                  data: {
                     email:$scope.email
                  }
              }).then(function(response) {
                     if(response.data["success"]){
                        alert("修改成功！");
                        $scope.refersh();
                     }
                     else{
                        alert(response.data["errorMsg"]);
                     }
              }, function(error) {
                  alert(error);
              });
            }
            $scope.changePassword = function() {
              if($scope.newPassword==$scope.confrimPassword){
                  $http({
                      method: 'POST',
                      url: '/api/admin/changepassword',
                      data: {
                         oldPassword:$scope.oldPassword,
                         password:$scope.newPassword
                      }
                  }).then(function(response) {
                         if(response.data["success"]){
                            alert("修改成功！");
                            window.location = "/api/admin/logout"; 
                         }
                         else{
                            alert(response.data["errorMsg"]);
                         }
                  }, function(error) {
                      alert("修改失败！");
                  });
              }
              else{
                alert("密码输入不一致！");
              }
            }
            $scope.refersh();
    })
